// app.js
var express = require('express');
var cookieParser = require('cookie-parser');
//var path = require('path');
//var bodyParser = require('body-parser');


const NODE_PORT = process.env.NODE_PORT || 3000;


var app = express();

// setup of the configuration of express.
app.use(cookieParser());
//app.use(bodyParser.urlencoded({extended : false}));


app.get("/cookie", function(req,res){
    console.log("created a cookie andn send to the browser");
    res.cookie("cookie_name", 'cookie_value').send('cookie sent !');
});

app.get("/cookie2", function(req,res){
    console.log("created a cookie andn send to the browser");
    res.cookie("cookie_name2", 'cookie_value2').send('cookie 2 sent !');
});

app.get("/cookie-age", function(req,res){
    console.log("created a cookie andn send to the browser");
    //res.cookie("cookie_name3", 'cookie_value3', {maxAge: 24 * 60 * 60 * 1000 }).send('cookie age sent !');
    res.cookie("cookie_name3", 'cookie_value3', {maxAge: 24 * 60 * 60 * 1000 })
    res.cookie("cookie_name4", 'cookie_value4', {maxAge: 24 * 60 * 60 * 1000 })
    res.json({ name : "test"});
});

app.get("/cookie-delete", function(req,res){
    res.clearCookie("cookie_name")
    res.clearCookie("cookie_name2")
    res.clearCookie("cookie_name3")
    res.clearCookie("cookie_name4")
    res.json({ name : "delete cookies"});
});

app.listen(NODE_PORT, function(){
    console.log("Server is running at port " + NODE_PORT);
})