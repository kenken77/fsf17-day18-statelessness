var express = require('express');

var bodyParser = require('body-parser');
var cookieSession = require('cookie-session');

var session = require('express-session')

const PORT = process.argv[2] || process.env.NODE_PORT || 3000;

var app = express();

app.use(
    cookieSession({
        secret: "azxsw23e%t1",
        resave: false,
        saveUninitialized: true,
        httpOnly: false
    })
);

app.use(function(req,res,next){
    if(! req.session.cart ){
        req.session.cart = [];
    }
    next();
})

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

app.post("/api/cart/add", function(req,res){
    req.session.cart.push({
        name: req.body.name,
        quantity: req.body.quantity
    })
    console.log("Cart - > "  + JSON.stringify(req.session.cart));
    res.status(202).end();
});

app.get("/api/cart/list", function(req,res){
    console.log("Req Cookie -> " + req.cookie);
    console.log("Res Cookie -> " + res.cookie);
    res.status(202).json(req.session.cart);
});

app.get("/api/cart/refresh", function(req,res){
    console.log("Refresh --> " + JSON.stringify(req.session.cart));
    res.json(req.session.cart);
})

app.post("/api/cart/checkout", function(req,res){
    req.session.cart = [];
    console.log("Checkout --> " + JSON.stringify(req.session.cart));
    res.status(202).end();
})

app.listen(PORT,function(){
    console.log("Server is running on ", PORT);
})